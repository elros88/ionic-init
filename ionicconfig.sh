#!/bin/bash

###############################################################################
#                                                                             #
#		SCRIPT PARA INICIAR AMBIENTE DE ITAU                          #
###############################################################################


# Limpiamos el ambiente

for i in `cordova plugin ls | grep '^[^ ]*' -o`; do cordova plugin rm $i; done

cordova platform remove android

sudo rm -r node_modules/
rm package-lock.json

npm cache clean --force 

# Instalamos las dependencias de con npm

npm install
npm install jshint
npm uninstall uglify-js
npm install uglify-js 

cordova platform add android@6.3.0

# Agregamos los plugins requeridos en el proyecto

cordova plugin add cordova-plugin-tag-manager
cordova plugin add com.lampa.startapp
cordova plugin add https://github.com/napolitano/cordova-plugin-intent
cordova plugin add cordova-plugin-actionsheet
cordova plugin add cordova-plugin-add-swift-support
cordova plugin add cordova-plugin-appavailability
cordova plugin add https://github.com/tomloprod/cordova-plugin-appminimize.git
cordova plugin add cordova-plugin-compat
cordova plugin add cordova-plugin-console
cordova plugin add cordova-plugin-customurlscheme
cordova plugin add cordova-plugin-datepicker
cordova plugin add cordova-plugin-decimal-keyboard
cordova plugin add cordova-plugin-device
cordova plugin add cordova-plugin-dialogs 
cordova plugin add cordova-plugin-file 
cordova plugin add cordova-plugin-geolocation 
cordova plugin add cordova-plugin-globalization 
cordova plugin add cordova-plugin-inappbrowser
cordova plugin add cordova-plugin-ionic-keyboard
cordova plugin add cordova-plugin-ionic-webview 
cordova plugin add cordova-plugin-nativestorage 
cordova plugin add cordova-plugin-network-information 
cordova plugin add cordova-plugin-secure-key-store 
cordova plugin add cordova-plugin-splashscreen 
cordova plugin add cordova-plugin-statusbar
cordova plugin add cordova-plugin-whitelist  

# Agregamos los plugins con configuracion especial y los desarrollados en Hexacta

cordova plugin add cordova-plugin-customurlscheme --variable URL_SCHEME="itauuyapp" 
cordova plugin add ../hx-plugins/cordova-plugin-facebook4 --save --variable APP_ID="194534911114147" --variable APP_NAME="itauuyapp" 
cordova plugin add ../hx-plugins/com.phonegap.plugins.nativesettingsopener
cordova plugin add ../hx-plugins/cordova-plugin-apprate
cordova plugin add ../hx-plugins/cordova-plugin-crosswalk-webview
cordova plugin add ../hx-plugins/cordova-plugin-decimal-keyboard
cordova plugin add ../hx-plugins/cordova-plugin-fingerprint-aio
cordova plugin add ../hx-plugins/hx-login

# Agregamos la plataforma y preparamos el proyecto
#cordova prepare android